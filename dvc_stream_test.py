import dvc.api
import pandas as pd

with dvc.api.open('Uganda/2015-16/Data/gsec1.dta',mode='rb') as dta:
    df = pd.read_stata(dta)
